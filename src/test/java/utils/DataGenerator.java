package utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Properties;
import java.util.Random;

import static utils.Constants.PATH_TO_COUNT_PROPERTIES;


public class DataGenerator {


    private static String countEmail;
    private static SecureRandom rnd = new SecureRandom();
    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static final String EXISTING_EMAIL = "fatimagulomova@gmail.com";
    public static final String EXISTING_PASSWORD = "RlxY/sDU";


    public static String[] nams = {"Альвиан", "Анатолий", "Андрей", "Антон", "Антонин", "Потап", "Пров", "Прокопий", "Протасий", "Прохор", "Иакинф",
            "Иван", "Игнатий", "Игорь", "Илья", "Иннокентий", "Святополк", "Святослав", "Севастьян", "Семён", "Сергей", "Сильвестр", "Матвей", "Мелентий",
            "Мирон", "Дмитрий", "Добрыня", "Филат", "Филипп", "Фома", "Влас", "Всеволод", "Вячеслав", "Леонид", "Леонтий", "Ириней", "Исидор",};


    public static String[] surnams = {"Лазарев", "Медведев", "Ершов", "Никитин", "Соболев", "Рябов", "Поляков", "Цветков", "Данилов", "Жуков", "Фролов",
            "Журавлёв", "Николаев", "Крылов", "Максимов", "Сидоров", "Осипов", "Белоусов", "Федотов", "Дорофеев", "Егоров", "Матвеев", "Бобров", "Дмитриев",
            "Калинин", "Анисимов", "Петухов", "Антонов", "Тимофеев", "Никифоров", "Веселов", "Филиппов", "Марков", "Большаков", "Суханов", "Миронов", "Ширяев",
            "Александров", "Коновалов", "Шестаков", "Казаков", "Ефимов", "Денисов", "Громов", "Фомин", "Давыдов", "Мельников", "Орехов", "Ефремов", "Исаев",
            "Евдокимов", "Калашников", "Кабанов", "Носков", "Юдин", "Кулагин",};

    public static String[] operatorCode = {"094", "067", "068", "096", "097", "098", "050", "066", "095", "099", "063", "093", "091", "092"};


    public static String[] addresses = {"№117, бул. Перова, дом 15", "№289, бул. Чоколовский , дом 1", "№7413, бул. Чоколовский, дом 39",
            "№271, майдан Майдан Независимости, квартал 3014", "№364, пер. Политехнический, дом 1/33-А", "№7189, пер. Политехнический, дом 1/37",
            "№312, ул. Леваневского, дом 4-Д", "№242, ул. Шевченка, дом 35/1", "№20, ул. Шевченко, дом 2/1", "№237, бул. Шевченка, дом 345/10",
            "№236, ул. Вернигоры, дом 4", "№130, ул. Небесной Сотни, дом 57/3", "№7404, ул. Смелянская, дом 23", "№375, ул. Смелянская, дом 40",
            "№235, ул. Смилянская, дом 42/3", "№344, ул. Сумгаитская, дом 24", "№720, ул. Сумгаитская, дом 24", "№240, ул. Небесной Сотни, дом 14",
            "№43, ул. Небесной Сотни, дом 14", "№396, ул. Хлебная, дом. 16", "№186, ул. Золочевская, дом 20", "№403, ул. Рождественская, дом 33",
            "№190, ул. С. Грицевца, дом 29-Б", "№351, ул. Сумская, дом 41/2", "№217, шоссе Салтивское, дом 147", "№180, ул. Рабочая, дом 77",
            "№175, ул. Шолохова, дом 1", "№228, шоссе Донецкое, дом 2-Б", "№83, ул. Героев Чернобыльцев, дом 1", "№395, ул. Европейская, дом 38",
            "№270, ул. Зиньковская , дом 4-А", "№192, ул. Ивана Мазепы, дом 37", "№7232, ул. Небесной Сотни, дом 114", "№36, ул. Соборности, дом 79",
            "№183, ул. Шевченка, дом 39-А", "№7273, ул. Шевченко, дом 54",};

    public static String[] cities = {"Винница", "Жмеринка", "Луцк", "Днепродзержинск", "Днепропетровск", "Кривой Рог", "Никополь",
            "Павлоград", "Артемовск", "Горловка", "Дзержинск", "Донецк", "Енакиево", "Константиновка", "Краматорск", "Макеевка",
            "Мариуполь", "Славянск", "Торез", "Харцызск", "Бердичев", "Житомир", "Мукачево", "Ужгород", "Бердянск", "Запорожье",
            "Мукачево", "Ивано", "Ивано-Франковск", "Белая Церковь", "Борисполь", "Бровары", "Ирпень", "Александрия", "Кировоград",
            "Алчевск", "Красный Луч", "Лисичанск", "Луганск", "Свердловск", "Северодонецк", "Стаханов", "Львов", "Жовква", "Николаев",
            "Измаил", "Ильичевск", "Одесса", "Комсомольск", "Кременчуг", "Полтава", "Дубно", "Ровно", "Конотоп", "Сумы", "Шостка",
            "Тернополь", "Лозовая", "Харьков", "Каховка", "Херсон", "Каменец-Подольский", "Хмельницкий", "Смела", "Умань", "Черкассы",
            "Нежин", "Чернигов", "Черновцы",};

    public static String[] products = {"Умные часы", "Phone XS", "Galaxy S9", "Планшеты", "Квадрокоптеры", "Аудио",
            "Фото", "Часы", "Смартфоны", "Видео"};


    public static int randomInt(int max, int min) {
        max -= min;
        return (int) ((Math.random() * ++max) + min);
    }

    public static int generateRandomDigits(int n) {
        int m = (int) Math.pow(10, n - 1);
        return m + new Random().nextInt(9 * m);
    }


    public static String randomPassword(int max, int min) {
        int len = randomInt(max, min);
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


    public static String randomName() {
        Random randomGenerator = new Random();
        int n = randomGenerator.nextInt(nams.length);
        return nams[n];
    }

    public static String randomSurname() {
        Random randomGenerator = new Random();
        int s = randomGenerator.nextInt(surnams.length);
        return surnams[s];
    }

    public static String randomPhoneNumber() {
        Random randomGenerator = new Random();
        int n = randomGenerator.nextInt(randomInt(14, 1));
        String codeRandom = operatorCode[n];
        return codeRandom + generateRandomDigits(7);
    }

    public static String randomAddress() {
        Random randomGenerator = new Random();
        int n = randomGenerator.nextInt(addresses.length);
        return addresses[n];
    }

    public static String randomProducts() {
        Random randomGenerator = new Random();
        int n = randomGenerator.nextInt(products.length);
        return products[n];
    }

    public static String randomCity() {
        Random randomGenerator = new Random();
        int n = randomGenerator.nextInt(cities.length);
        return cities[n];
    }


    public static String getEmail() {
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try {
            fileInputStream = new FileInputStream(PATH_TO_COUNT_PROPERTIES);
            prop.load(fileInputStream);
            countEmail = String.join("", "fatimagulomova+", prop.getProperty("count"), "@gmail.com");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return countEmail;
    }

    public static String getCountEmail() {
        String count = "";
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try {
            fileInputStream = new FileInputStream(PATH_TO_COUNT_PROPERTIES);
            prop.load(fileInputStream);
            count = prop.getProperty("count");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static void setNewCountEmail() {
        Integer count = Integer.valueOf(getCountEmail()) + 1;
        Properties prop = new Properties();
        OutputStream output = null;
        try {
            output = new FileOutputStream(PATH_TO_COUNT_PROPERTIES);
            prop.setProperty("count", count.toString());
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
