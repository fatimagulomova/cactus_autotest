package utils;

public class Constants {

    public static final String ERROR_NOT_EQUALS_URL = "NOT equals URL";
    public static final String ERROR_MESSAGE_NOT_MATCH = "Error message does not match";
    public static final String PATH_TO_PROPERTIES = "src/main/resources/config.properties";
    public static final String PATH_TO_COUNT_PROPERTIES = "src/main/resources/count.properties";

    /*HOME Page*/
    public static final String NOT_SEARCH_RESULT= "Does not match the search";
    public static final String NOT_RESULT_MESSAGE= "Ничего не найдено  ";


    /* RegistrationPage*/
    public static final String ERROR_PHONE_NUMBER = "Вы должны указать номер телефона.\nphone_mobile не корректно.";
    public static final String ERROR_PASSWORD = "passwd не корректно.";
    public static final String ERROR_NAME = "Имя не корректно.";
    public static final String ERROR_SURNAME = "Фамилия не корректно.";


    /* AuthenticationPage*/
    public static final String ERROR_EXISTING_USER = "Учетная запись с этой электронной почтой уже зарегистрирована. Введите ваш пароль или запросите новый.";
    public static final String ERROR_AUTHENTIFICATION = "Ошибка авторизации.";
    public static final String ERROR_NEED_EMAIL = "Необходим e-mail.";
    public static final String ERROR_NEED_PASSWORD = "Необходимо указать пароль.";


    /*MENU Page*/
    public static final String ADDED_TO_CART_MESSAFE = "Товар был добавлен в корзину";

}
