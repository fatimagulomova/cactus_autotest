package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static utils.Constants.ERROR_NOT_EQUALS_URL;

public class MyAccountPage extends BasePage {
    public MyAccountPage(EventFiringWebDriver driver) {
        super(driver);
    }

    private String baseUrlSI = "https://cactus.kh.ua/my-account";

    @FindBy(id = "email_create")
    private WebElement registrationEmailField;


    @FindBy(xpath = "//*[contains(@class,'myaccount_address')]/a")
    private WebElement adressLink;


    public MyAccountPage verificationOfValidityUr() {
        Assert.assertEquals(ERROR_NOT_EQUALS_URL, baseUrlSI, this.driver.getCurrentUrl());
        return this;
    }


    public MyAccountPage enterEmailText(String emailText) {
        registrationEmailField.sendKeys(emailText);
        return this;
    }

    public MyAccountPage clickAddressLink() {
        adressLink.click();
        return this;
    }

}
