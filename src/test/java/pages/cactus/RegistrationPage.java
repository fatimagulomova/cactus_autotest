package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static org.openqa.selenium.Keys.HOME;
import static utils.Constants.*;

public class RegistrationPage extends BasePage {
    public RegistrationPage(EventFiringWebDriver driver) {
        super(driver);
    }


    @FindBy(id = "email_create")
    private WebElement registrationEmailField;

    @FindBy(id = "customer_firstname")
    private WebElement nameField;

    @FindBy(id = "customer_lastname")
    private WebElement surnameField;

    @FindBy(id = "phone_mobile")
    private WebElement phoneNumberField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "submitAccount")
    private WebElement submitButton;

    @FindBy(className = "alert-danger")
    private WebElement errorMessages;


    public RegistrationPage enterNameText(String name) {
        nameField.sendKeys(name);
        return this;
    }

    public RegistrationPage enterSurnameText(String surname) {
        surnameField.sendKeys(surname);
        return this;
    }

    public RegistrationPage enterPhoneNumber(String phoneNumber) {
        phoneNumberField.sendKeys(HOME);
        phoneNumberField.sendKeys(phoneNumber);
        return this;
    }

    public RegistrationPage enterPassword(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public RegistrationPage clickSubmitButton() {
        submitButton.click();
        return this;
    }


    public RegistrationPage checkErrorMessagePhoneNumber() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_PHONE_NUMBER, errorMessages.getText());
        return this;
    }

    public RegistrationPage checkErrorMessagePassword() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_PASSWORD, errorMessages.getText());
        return this;
    }

    public RegistrationPage checkErrorMessageName() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_NAME, errorMessages.getText());
        return this;
    }

    public RegistrationPage checkErrorSurname() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_SURNAME, errorMessages.getText());
        return this;
    }

}
