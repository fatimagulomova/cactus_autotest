package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static utils.Constants.ERROR_NOT_EQUALS_URL;

public class PasswordRecoveryPage extends BasePage {
    public PasswordRecoveryPage(EventFiringWebDriver driver) {
        super(driver);
    }


    private String baseUrlSI = "https://cactus.kh.ua/password-recovery";



    public PasswordRecoveryPage verificationOfValidityUr() {
        Assert.assertEquals(ERROR_NOT_EQUALS_URL, baseUrlSI, this.driver.getCurrentUrl());
        return this;
    }

}
