package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static utils.Constants.ERROR_NOT_EQUALS_URL;

public class AddressesPage extends BasePage {


    private String baseUrlSI = "https://cactus.kh.ua/addresses";

    public AddressesPage(EventFiringWebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[contains(@class,'alert alert-warning')]/a")
    private WebElement addNewAddress;

    @FindBy(id = "address1")
    private WebElement addressFild;

    @FindBy(id = "city")
    private WebElement cityFild;

    @FindBy(id = "alias")
    private WebElement aliasFild;

    @FindBy(id = "submitAddress")
    private WebElement submitAddressButton;

    @FindBy(xpath = "//*[contains(@class,'address_update')]/a[1]")
    private WebElement updateAddressButton;

    @FindBy(xpath = "//*[contains(@class,'address_update')]/a[2]")
    private WebElement deleteAddressButton;

    public AddressesPage verificationOfValidityUr() {
        Assert.assertEquals(ERROR_NOT_EQUALS_URL, baseUrlSI, this.driver.getCurrentUrl());
        return this;
    }

    public AddressesPage clickNewAddress() {
        addNewAddress.click();
        return this;
    }

    public AddressesPage clickUpdateAddress() {
        updateAddressButton.click();
        return this;
    }

    public AddressesPage clickDeleteAddress() {
        deleteAddressButton.click();
        return this;
    }

    public AddressesPage clickSubmitButton() {
        submitAddressButton.click();
        return this;
    }


    public AddressesPage enterAddress(String addresslText) {
        addressFild.clear();
        addressFild.sendKeys(addresslText);
        return this;
    }

    public AddressesPage isVisibleAddNewAddressLink() {
        waitUntilConditionsWebElement(addNewAddress,1);
        return this;
    }


    public AddressesPage isVisibleUpdateAddressButton() {
        waitUntilConditionsWebElement(updateAddressButton,1);
        return this;
    }

    public AddressesPage enterCity(String cityText) {
        cityFild.clear();
        cityFild.sendKeys(cityText);
        return this;
    }

    public AddressesPage enterEmail(String emailText) {
        addressFild.clear();
        addressFild.sendKeys(emailText);
        return this;
    }

    public AddressesPage enterAliasAdress(String aliasAdress) {
        aliasFild.clear();
        aliasFild.sendKeys(aliasAdress);
        return this;
    }

}
