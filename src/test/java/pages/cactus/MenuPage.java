package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static utils.Constants.ADDED_TO_CART_MESSAFE;
import static utils.Constants.ERROR_MESSAGE_NOT_MATCH;
import static utils.Constants.ERROR_PASSWORD;

public class MenuPage extends BasePage {

    public MenuPage(EventFiringWebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".main-menu>li:nth-child(8) a")
    private WebElement photoLink;

    @FindBy(css = ".main-menu>li:nth-child(8) > ul>li a")
    private WebElement actionCameraLink;

    @FindBy(css = ".main-menu>li:nth-child(8) > ul>li:first-child ul li:nth-child(1) a")
    private WebElement goProLink;

    @FindBy(xpath = "//*[@id=\"center_column\"]/div[1]/div[2]/div/div/div[4]/div[1]/div/div[5]/div[3]/a")
    private WebElement buyProduct;

    @FindBy(css = ".icon-check:first-child")
    private WebElement addedToCartMessage;

    @FindBy(className = "cross")
    private WebElement closeWindowButton;

    public MenuPage clickPhotoLink() {
        photoLink.click();
        return this;
    }

    public MenuPage clickGoProLink() {
        goProLink.click();
        return this;
    }

    public MenuPage clickCloseWindowButton() {
        closeWindowButton.click();
        return this;
    }

    public MenuPage clickActionCameraLink() {
        goProLink.click();
        return this;
    }

    public MenuPage clickBuyProduct() {
        buyProduct.click();
        return this;
    }

    public MenuPage checkAddedToCartMessage() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ADDED_TO_CART_MESSAFE, addedToCartMessage.getText());
        return this;
    }

}
