package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static utils.Constants.*;

public class AuthenticationPage extends BasePage {
    public AuthenticationPage(EventFiringWebDriver driver) {
        super(driver);
    }

    private String baseUrlSI = "https://cactus.kh.ua/authentication?back=my-account";

    @FindBy(id = "email")
    private WebElement emailFild;

    @FindBy(id = "passwd")
    private WebElement passwordFild;

    @FindBy(id = "SubmitLogin")
    private WebElement loginButton;

    @FindBy(id = "email_create")
    private WebElement registrationEmailField;

    @FindBy(id = "SubmitCreate")
    private WebElement createButton;

    @FindBy(xpath = "//*[contains(@class, 'lost_password form-group')]/a")
    private WebElement forgotPasswordLink;

    @FindBy(xpath = "//*[contains(@class, 'alert alert-danger')]/ol/li")
    private WebElement createAccountError;

    @FindBy(xpath = "//*[contains(@class, 'alert alert-danger')]/ul/li")
    private WebElement errorAuthenticationMessage;

    public AuthenticationPage openPage() throws InterruptedException {
        sleep(driver);
        this.driver.get(baseUrlSI);
        return this;
    }


    public AuthenticationPage verificationOfValidityUr() {
        Assert.assertEquals(ERROR_NOT_EQUALS_URL, baseUrlSI, this.driver.getCurrentUrl());
        return this;
    }

    public AuthenticationPage enterNewEmail(String newEmail) {
        registrationEmailField.clear();
        registrationEmailField.sendKeys(newEmail);
        return this;
    }

    public AuthenticationPage clickCreateButton() {
        createButton.click();
        return this;
    }

    public AuthenticationPage checkErrorExistingUser() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_EXISTING_USER, createAccountError.getText());
        return this;
    }

    public AuthenticationPage clickForgotPassword() {
        forgotPasswordLink.click();
        return this;
    }

    public AuthenticationPage enterEmail(String email) {
        emailFild.clear();
        emailFild.sendKeys(email);
        return this;
    }


    public AuthenticationPage enterPassword(String password) {
        passwordFild.clear();
        passwordFild.sendKeys(password);
        return this;
    }

    public AuthenticationPage clickLoginButton() {
        loginButton.click();
        return this;
    }

    public AuthenticationPage checkErrorAuthentication() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_AUTHENTIFICATION, errorAuthenticationMessage.getText());
        return this;
    }

    public AuthenticationPage checkErrorNeedEmail() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_NEED_EMAIL, errorAuthenticationMessage.getText());
        return this;
    }

    public AuthenticationPage checkErrorNeedPassword() {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH,
                ERROR_NEED_PASSWORD, errorAuthenticationMessage.getText());
        return this;
    }
}
