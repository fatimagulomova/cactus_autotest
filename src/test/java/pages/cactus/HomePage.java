package pages.cactus;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.BasePage;

import static utils.Constants.ERROR_MESSAGE_NOT_MATCH;
import static utils.Constants.ERROR_NOT_EQUALS_URL;
import static utils.Constants.NOT_RESULT_MESSAGE;

public class HomePage extends BasePage {

    public HomePage(EventFiringWebDriver driver) {
        super(driver);
    }

    private String baseUrlSI = "https://cactus.kh.ua/";

    @FindBy(id = "gs_tti50")
    private WebElement searhFieldMain;

    @FindBy(css = ".gs-no-results-result .gs-snippet")
    private WebElement noResultMessage;

    @FindBy(className = "login")
    private WebElement loginButton;

    @FindBy(className = "lighter")
    private WebElement resultSearchText;

    @FindBy(id = "search_query_block")
    private WebElement searchFild;

    @FindBy(id = "search_button")
    private WebElement searchButton;

    @FindBy(xpath = "//*[contains(@class, 'center_column')]/p")
    private WebElement noResultSearch;


    public HomePage openPage() throws InterruptedException {
        sleep(driver);
        this.driver.get(baseUrlSI);
        return this;
    }


    public HomePage verificationOfValidityUr() {
        Assert.assertEquals(ERROR_NOT_EQUALS_URL, baseUrlSI, this.driver.getCurrentUrl());
        return this;
    }

    public HomePage clickLoginButton() {
        loginButton.click();
        return this;
    }

    public HomePage enterTextFromSearch(String searchText) {
        searchFild.clear();
        searchFild.sendKeys(searchText);
        return this;
    }

    public HomePage clickSearchButton() {
        searchButton.click();
        return this;
    }

    public HomePage checkSearchResult(String searchText) {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH, "\"" + searchText + "\"", resultSearchText.getText());
        return this;
    }

    public HomePage checkNoResultSearch(String searchText) {
        Assert.assertEquals(ERROR_MESSAGE_NOT_MATCH, NOT_RESULT_MESSAGE+"\"" + searchText + "\"", noResultSearch.getText());
        return this;
    }

}
