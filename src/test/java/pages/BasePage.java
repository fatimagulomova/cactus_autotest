package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BasePage {


    public BasePage(EventFiringWebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        sleep(driver);
    }

    public EventFiringWebDriver driver;
    public static int counter = 0;


    public void sleep(EventFiringWebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
    }

    public boolean waitUntilConditionsWebElement(WebElement locator, int type) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 20)
                    .ignoring(StaleElementReferenceException.class, ElementNotVisibleException.class);
            if (type == 1) { //visible
                WebElement element = wait.until(ExpectedConditions.visibilityOf(locator));
            }
            if (type == 2) { //clickable
                WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
            }
            if (type == 3) { //disappear
                wait.until(ExpectedConditions.invisibilityOf(locator));
            }
        } catch (TimeoutException ex) {
            return false;
        }
        return true;
    }


    public boolean waitUntilConditionsBy(By locator, int time, int type) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            if (type == 0) { //exist
                wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
            }
            if (type == 1) { //visible
                wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            }
            if (type == 2) { //clickable
                wait.until(ExpectedConditions.elementToBeClickable(locator));
            }
            if (type == 3) { //disappear
                wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
            }
        } catch (TimeoutException ex) {
            return false;
        }
        return true;
    }

    public boolean waitUntilConditionsBy(WebElement locator, int time, int type) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            if (type == 1) { //clickable
                wait.until(ExpectedConditions.elementToBeClickable(locator));
            }
            if (type == 2) { //disappear
                wait.until(ExpectedConditions.invisibilityOf(locator));
            }
        } catch (TimeoutException ex) {
            return false;
        }
        return true;
    }

}
