package settings;

import listeners.MyEventListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;

public class TLDriverFactory {

    private static OptionsManager optionsManager = new OptionsManager();
    private static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    public static synchronized void setDriver(String browser) {
        if (browser.equals("firefox")) {
            tlDriver = ThreadLocal.withInitial(() -> new FirefoxDriver(optionsManager.getFirefoxOptions()));
        } else if (browser.equals("chrome")) {
            tlDriver.set(new ChromeDriver(optionsManager.getChromeOptions()));
        }
    }

    public static synchronized WebDriverWait getWait(WebDriver driver) {
        return new WebDriverWait(driver, 20);
    }

    public static synchronized EventFiringWebDriver getDriver() {
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(tlDriver.get());
        eventFiringWebDriver.register(new MyEventListener(Thread.currentThread().getId()));
        return eventFiringWebDriver;
    }

    public static synchronized void removeDriver() {
        if (tlDriver.get() != null) {
            tlDriver.get().quit();
            tlDriver.remove();
        }
    }

}