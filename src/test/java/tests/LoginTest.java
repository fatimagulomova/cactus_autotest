package tests;


import listeners.MyInvokedMethodListener;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.cactus.*;
import settings.TLDriverFactory;

import static utils.DataGenerator.*;

@RunWith(JUnit38ClassRunner.class)
@Listeners(MyInvokedMethodListener.class)
public class LoginTest {


    @Test(priority = 0)
    public void validLogin() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(EXISTING_EMAIL)
                .enterPassword(EXISTING_PASSWORD)
                .clickLoginButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr();
    }


    @Test(priority = 1)
    public void loginWithUnregisteredUser() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(getEmail())
                .enterPassword(randomPassword(7, 5))
                .clickLoginButton()
                .checkErrorAuthentication();
    }


    @Test(priority = 2)
    public void invalidLoginEmptyEmail() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail("")
                .enterPassword(randomPassword(7, 5))
                .clickLoginButton()
                .checkErrorNeedEmail();
    }


    @Test(priority = 3)
    public void invalidLoginEmptyPassword() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(getEmail())
                .enterPassword("")
                .clickLoginButton()
                .checkErrorNeedPassword();
    }

    @Test(priority = 4)
    public void validLoginAndAddAddress() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(EXISTING_EMAIL)
                .enterPassword(EXISTING_PASSWORD)
                .clickLoginButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr()
                .clickAddressLink();
        new AddressesPage(driver)
                .verificationOfValidityUr()
                .clickNewAddress()
                .enterAddress(randomAddress())
                .enterCity(randomCity())
                .enterAliasAdress(randomCity())
                .clickSubmitButton()
                .isVisibleUpdateAddressButton();
    }


    @Test(priority = 5)
    public void validLoginAndChangeAddress() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(EXISTING_EMAIL)
                .enterPassword(EXISTING_PASSWORD)
                .clickLoginButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr()
                .clickAddressLink();
        new AddressesPage(driver)
                .verificationOfValidityUr()
                .clickUpdateAddress()
                .enterAddress(randomAddress())
                .enterCity(randomCity())
                .enterAliasAdress(randomCity())
                .clickSubmitButton()
                .isVisibleUpdateAddressButton();
    }


    @Test(priority = 6)
    public void validLoginAndDeleteAddress() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(EXISTING_EMAIL)
                .enterPassword(EXISTING_PASSWORD)
                .clickLoginButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr()
                .clickAddressLink();
        new AddressesPage(driver)
                .verificationOfValidityUr()
                .clickDeleteAddress()
                .isVisibleAddNewAddressLink();
    }


}
