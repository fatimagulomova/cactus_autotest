package tests;


import listeners.MyInvokedMethodListener;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.cactus.AuthenticationPage;
import pages.cactus.HomePage;
import pages.cactus.PasswordRecoveryPage;
import settings.TLDriverFactory;

@RunWith(JUnit38ClassRunner.class)
@Listeners(MyInvokedMethodListener.class)
public class PasswordRecoveryTest {



    @Test()
    public void validResetPassword() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .clickForgotPassword();
        new PasswordRecoveryPage(driver)
                .verificationOfValidityUr();

    }


}
