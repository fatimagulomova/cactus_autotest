package tests;


import listeners.MyInvokedMethodListener;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.cactus.AuthenticationPage;
import pages.cactus.HomePage;
import pages.cactus.MenuPage;
import pages.cactus.MyAccountPage;
import settings.TLDriverFactory;

import static utils.DataGenerator.EXISTING_EMAIL;
import static utils.DataGenerator.EXISTING_PASSWORD;

@RunWith(JUnit38ClassRunner.class)
@Listeners(MyInvokedMethodListener.class)
public class AddProductToCartTest {

    @Test()
    public void validAddProductToCart() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterEmail(EXISTING_EMAIL)
                .enterPassword(EXISTING_PASSWORD)
                .clickLoginButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr();
        new HomePage(driver)
                .openPage();
        new MenuPage(driver)
                .clickBuyProduct()
                .clickCloseWindowButton();
    }
}
