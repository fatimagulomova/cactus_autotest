package tests;


import listeners.MyInvokedMethodListener;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.cactus.AuthenticationPage;
import pages.cactus.HomePage;
import settings.TLDriverFactory;

import static utils.DataGenerator.EXISTING_EMAIL;
import static utils.DataGenerator.randomPassword;
import static utils.DataGenerator.randomProducts;

@RunWith(JUnit38ClassRunner.class)
@Listeners(MyInvokedMethodListener.class)
public class ProductSearchTest {

    @Test()
    public void validSearch() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        String productName = randomProducts();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .enterTextFromSearch(productName)
                .clickSearchButton()
                .checkSearchResult(productName);
    }

    @Test()
    public void validUpperCaseSearch() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        String productName = randomProducts().toUpperCase();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .enterTextFromSearch(productName)
                .clickSearchButton()
                .checkSearchResult(productName);
    }


    @Test()
    public void validLowerCaseSearch() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        String productName = randomProducts().toLowerCase();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .enterTextFromSearch(productName)
                .clickSearchButton()
                .checkSearchResult(productName);
    }


    @Test()
    public void invalidTextSearch() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        String invalidProductName = randomPassword(6,4);
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .enterTextFromSearch(invalidProductName)
                .clickSearchButton()
                .checkNoResultSearch(invalidProductName);
    }

    @Test()
    public void invalidEmptySearch() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        String invalidProductName = " ";
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .enterTextFromSearch(invalidProductName)
                .clickSearchButton()
                .checkNoResultSearch(invalidProductName);
    }


}
