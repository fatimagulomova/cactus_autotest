package tests;

import listeners.MyInvokedMethodListener;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.cactus.*;
import settings.TLDriverFactory;

import static utils.DataGenerator.*;
import static utils.DataGenerator.randomCity;


@RunWith(JUnit38ClassRunner.class)
@Listeners(MyInvokedMethodListener.class)
public class RegistrationTest {


    @Test()
    public void validRegistrationNewUser() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(getEmail())
                .clickCreateButton();
        setNewCountEmail();
        new RegistrationPage(driver)
                .enterNameText(randomName())
                .enterSurnameText(randomSurname())
                .enterPhoneNumber(randomPhoneNumber())
                .enterPassword(randomPassword(7,5))
                .clickSubmitButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr();

    }


    @Test()
    public void validRegistrationNewUserAndFillAddress() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(getEmail())
                .clickCreateButton();
        setNewCountEmail();
        new RegistrationPage(driver)
                .enterNameText(randomName())
                .enterSurnameText(randomSurname())
                .enterPhoneNumber(randomPhoneNumber())
                .enterPassword(randomPassword(7,5))
                .clickSubmitButton();
        new MyAccountPage(driver)
                .verificationOfValidityUr()
                .clickAddressLink();
        new AddressesPage(driver)
                .verificationOfValidityUr()
                .clickNewAddress()
                .enterAddress(randomAddress())
                .enterCity(randomCity())
                .enterAliasAdress(randomCity())
                .clickSubmitButton()
                .isVisibleUpdateAddressButton();
    }


    @Test()
    public void registrationNewUserWithInvalidPassword() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(getEmail())
                .clickCreateButton();
        new RegistrationPage(driver)
                .enterNameText(randomName())
                .enterSurnameText(randomSurname())
                .enterPhoneNumber(randomPhoneNumber())
                .enterPassword(randomPassword(4,3))
                .clickSubmitButton()
                .checkErrorMessagePassword();
    }


    @Test()
    public void registrationNewUserWithInvalidPhoneNumber() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(getEmail())
                .clickCreateButton();
        new RegistrationPage(driver)
                .enterNameText(randomName())
                .enterSurnameText(randomSurname())
                .enterPhoneNumber(String.valueOf(generateRandomDigits(5)))
                .enterPassword(randomPassword(7,5))
                .clickSubmitButton()
                .checkErrorMessagePhoneNumber();
    }


    @Test()
    public void registrationNewUserWithInvalidName() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(getEmail())
                .clickCreateButton();
        new RegistrationPage(driver)
                .enterNameText(String.valueOf(generateRandomDigits(3)))
                .enterSurnameText(randomSurname())
                .enterPhoneNumber(String.valueOf(randomPhoneNumber()))
                .enterPassword(randomPassword(7,5))
                .clickSubmitButton()
                .checkErrorMessageName();
    }


    @Test()
    public void registrationNewUserWithInvalidSurname() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(getEmail())
                .clickCreateButton();
        new RegistrationPage(driver)
                .enterNameText(randomName())
                .enterSurnameText(String.valueOf(generateRandomDigits(3)))
                .enterPhoneNumber(randomPhoneNumber())
                .enterPassword(randomPassword(7,5))
                .clickSubmitButton()
                .checkErrorSurname();
    }


    @Test()
    public void registrationExistingUser() throws InterruptedException {
        EventFiringWebDriver driver = TLDriverFactory.getDriver();
        new HomePage(driver)
                .openPage()
                .verificationOfValidityUr()
                .clickLoginButton();
        new AuthenticationPage(driver)
                .verificationOfValidityUr()
                .enterNewEmail(EXISTING_EMAIL)
                .clickCreateButton()
                .checkErrorExistingUser();
    }


}
