package listeners;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import java.io.File;
import java.io.IOException;

public class MyEventListener extends AbstractWebDriverEventListener {

    Long numberThread;

    public MyEventListener(Long numberThread) {
        this.numberThread = numberThread;
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("Thread Local number: " + numberThread + " .Web Item Search: " + by);
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("Thread Local number: " + numberThread + " .Web Element with this locator: " + by + " found!");
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        System.out.println(throwable);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        File tmp = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File screen = new File(new File("src/test/png"), "screen - " + System.currentTimeMillis() + "num" + numberThread + ".png");
        try {
            com.google.common.io.Files.copy(tmp, screen);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
