package listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import settings.TLDriverFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static utils.Constants.PATH_TO_PROPERTIES;


public class MyInvokedMethodListener  implements IInvokedMethodListener {
    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(method.isTestMethod());
        if (method.isTestMethod()) {
            FileInputStream fileInputStream;
            Properties prop = new Properties();
            try {
                fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
                prop.load(fileInputStream);
                System.setProperty("webdriver.chrome.driver", prop.getProperty("config.chrome"));
                System.setProperty("webdriver.gecko.driver", prop.getProperty("config.gecko"));
                System.out.println("Test Method BeforeInvocation is started. " + Thread.currentThread().getId());
                String browserName = method.getTestMethod().getXmlTest().getLocalParameters().get("browser");
                if(browserName==null){
                    browserName = prop.getProperty("default_browser");
                }
                TLDriverFactory.setDriver(browserName);
            } catch (NullPointerException e) {
                System.out.println("Add the file config.property to the resource folder and specify the path to the driver");
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Driver not found");
            }
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            System.out.println("Test Method AfterInvocation is started. " + Thread.currentThread().getId());
            TLDriverFactory.removeDriver();
        }
    }

}
